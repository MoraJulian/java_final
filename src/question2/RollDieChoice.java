package question2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class RollDieChoice implements EventHandler<ActionEvent>{
	private Text rollResult;
	private TextField playerBet;
	private String playerChoice;
	private RollDieGame game;
	private Text moneyText;
	
	
	public RollDieChoice(Text moneyText,Text rollResult, String playerChoice, RollDieGame game,TextField playerBet) {
		this.rollResult = rollResult;
		this.playerChoice = playerChoice;
		this.game = game;
		this.playerBet = playerBet;
		this.moneyText = moneyText;
		
	}


	@Override
	public void handle(ActionEvent e) {
		
		//Plays the round
		String result = game.playRound(playerBet,playerChoice);
		
		//Sets the result in the result field
		rollResult.setText(result);
		//Sets the players money in the money field
		moneyText.setText("Your Money: " +game.getMoney());
		
	}
	
}
