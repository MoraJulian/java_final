package question2;
import java.util.Random;

import javafx.scene.control.TextField;

public class RollDieGame {

	
	Random ran = new Random();
	int money = 500;
	String dieSize = "";
	int playerBet;
	
	public String getMoney() {
		return String.valueOf(this.money);
	}
	
	
	/**
	 * This method takes the players money bet and their choice. It creates the roll and its value
	 * then compares the results with the player choice. The money variable is then handled accordingly.
	 * @param playerBetTF
	 * @param playerChoice
	 * @return
	 */
	public String playRound(TextField playerBetTF,String playerChoice) {
		
		//Makes sure the playerBet is a number
		try {
			playerBet = Integer.parseInt(playerBetTF.getText());
		}
		
		catch(Exception e){
			return "You must enter a number and NOTHING ELSE";
		}
		
		
		
		//Makes sure the player is betting money he has
		if(playerBet > this.money) {
			return "You're betting with money you don't have. Try again";
		}
		
		//The die roll result
		int dieRoll = ran.nextInt(6)+1;
		//Then assigns their size using the result 
		if(dieRoll<=3) {
			this.dieSize = "small";
		}
		else {
			this.dieSize = "large";
		}
		
		//Checks if the player bet right (compares player choice and dieSize)
		if(playerChoice.toLowerCase().equals(this.dieSize)) {
			this.money += playerBet;
			return "You rolled: " +dieRoll+""
					+ "\nYou won the bet!" ;
		}
		else {
			this.money -= playerBet;
			return "You rolled: " +dieRoll+""
			+ "\nYou lost the bet" ;
		}
		
	}
}