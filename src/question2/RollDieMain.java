package question2;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RollDieMain extends Application {
	RollDieGame game = new RollDieGame();
	@Override
	public void start(Stage stage) { 
		//container and layout 
		Group root = new Group(); 
		HBox row1 = new HBox();
		HBox row2 = new HBox();
		HBox row3 = new HBox();
		HBox row4 = new HBox();
		VBox container = new VBox();
		
		//Buttons for player choices
		Button smallbttn = new Button("small");
		Button largebttn = new Button("big");
		
		//Player input field
		TextField bet = new TextField();
		
		//Result of round and money value
		Text resultText = new Text();
		Text moneyTextValue = new Text("Your Money: 500");
		
		row1.getChildren().add(bet);
		row2.getChildren().addAll(smallbttn,largebttn);
		row3.getChildren().add(resultText);
		row4.getChildren().add(moneyTextValue);
		container.getChildren().addAll(row1,row2,row3,row4);
		root.getChildren().add(container);
		
		
		//creating the Events
		RollDieChoice smallChoice = new RollDieChoice(moneyTextValue,resultText,"small",game,bet);
		RollDieChoice largeChoice = new RollDieChoice(moneyTextValue,resultText,"small",game,bet);
		
		//Assigning events to the buttons
		smallbttn.setOnAction(smallChoice);
		largebttn.setOnAction(largeChoice);
		
		//small.setOnAction(small);
		//small.setOnAction(large);
		

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 400, 300); 
		scene.setFill(Color.WHITE);

		//associate scene to stage and show
		stage.setTitle("BettingGame"); 
		stage.setScene(scene); 
		stage.show(); 
	} 
       public static void main(String[] args) {
                Application.launch(args);
       }   
}

