package java_final;

public class Recursion {

	public static void main(String[] args) {
		int[] test = {12,3,4,32,23,55,7,32,6,2,7,8,23,6};
		System.out.println(recursiveCount(test,5));
		
	}
	
	public static int recursiveCount(int[] numbers, int n) {
		if(numbers.length > n) {
			if(numbers[n] >= 20 && n%2 !=0) {
				return recursiveCount(numbers, n+1)+1;
			}
			return recursiveCount(numbers,n+1);
		}
		System.out.println("end of numbers");
		return 0 ;
	}

	
}

